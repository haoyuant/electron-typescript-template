// package: 
// file: model.proto

import * as jspb from "google-protobuf";

export class Animal extends jspb.Message {
  getSpieces(): string;
  setSpieces(value: string): void;

  getName(): string;
  setName(value: string): void;

  getAge(): number;
  setAge(value: number): void;

  clearTagsList(): void;
  getTagsList(): Array<string>;
  setTagsList(value: Array<string>): void;
  addTags(value: string, index?: number): string;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Animal.AsObject;
  static toObject(includeInstance: boolean, msg: Animal): Animal.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Animal, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Animal;
  static deserializeBinaryFromReader(message: Animal, reader: jspb.BinaryReader): Animal;
}

export namespace Animal {
  export type AsObject = {
    spieces: string,
    name: string,
    age: number,
    tagsList: Array<string>,
  }
}

