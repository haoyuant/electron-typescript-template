class Device { 
    model: string;
    serialNumber: string;
    isRunning: boolean;

    constructor(model: string, serialNumber: string) {
        this.model  = model;
        this.serialNumber = serialNumber;
        this.isRunning = false;
    }

    start() {
        this.isRunning = true;
    }

    stop() {
        this.isRunning = false;
    }

    check() : boolean {
        return this.isRunning;
    }
}

export {Device};
