// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.
import Dygraph from 'dygraphs';
import { Device } from './lib/device';
import { Animal } from './models/model_pb';
// or, if you don't want to use ES6 imports:
// const Dygraph = require('dygraphs');

const g = new Dygraph(document.getElementById("graphdiv"),
  `Date,A,B
2016/01/01,10,20
2016/07/01,20,10
2016/12/31,40,30
`, {
    fillGraph: true
  });

let device = new Device("PoE", "123456789");
console.log(device.check());
device.start();
console.log(device.check());
device.stop();
console.log(device.check());

let animal = new Animal();
animal.setSpieces("dog");
animal.setName("hdy");
animal.setAge(1);
animal.setTagsList(["hentai", "bald"]);

console.log(animal.getName());
if (true) {
  console.log("!");
}