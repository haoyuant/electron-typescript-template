"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.
const dygraphs_1 = __importDefault(require("dygraphs"));
const device_1 = require("./lib/device");
const model_pb_1 = require("./models/model_pb");
// or, if you don't want to use ES6 imports:
// const Dygraph = require('dygraphs');
const g = new dygraphs_1.default(document.getElementById("graphdiv"), `Date,A,B
2016/01/01,10,20
2016/07/01,20,10
2016/12/31,40,30
`, {
    fillGraph: true
});
let device = new device_1.Device("PoE", "123456789");
console.log(device.check());
device.start();
console.log(device.check());
device.stop();
console.log(device.check());
let animal = new model_pb_1.Animal();
animal.setSpieces("dog");
animal.setName("hdy");
animal.setAge(1);
animal.setTagsList(["hentai", "bald"]);
console.log(animal.getName());
//# sourceMappingURL=renderer.js.map