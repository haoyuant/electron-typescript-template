"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Device {
    constructor(model, serialNumber) {
        this.model = model;
        this.serialNumber = serialNumber;
        this.isRunning = false;
    }
    start() {
        this.isRunning = true;
    }
    stop() {
        this.isRunning = false;
    }
    check() {
        return this.isRunning;
    }
}
exports.Device = Device;
//# sourceMappingURL=device.js.map